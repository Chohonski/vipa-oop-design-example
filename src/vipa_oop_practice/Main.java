/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vipa_oop_practice;

import Player.*;
import Team.*;
import Team.League.league;
import java.util.ArrayList;

/**
 *
 * @author Mitch Chohon
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Player> Players = new ArrayList<>();
        ArrayList<Team> Teams = new ArrayList<>();
        
        BaseballTeam LADodgers = new BaseballTeam("LA Dodgers", "Los Angeles", "", league.A);
        FootballTeam DenverBroncos = new FootballTeam("Denver Broncos", "Denver", "Bronco", league.B);
        SoccerTeam DoaneTigersSoccer = new SoccerTeam("Doane Tigers", "Crete", "Tiger", league.D1);
        
        BaseballPlayer KershawClayton = new BaseballPlayer("Clayton Kershaw", LADodgers, "Pitcher", "Highland Park High School", "N/A", 243);
        BaseballPlayer TurnerJustin = new BaseballPlayer("Justin Turner", LADodgers, "In-fielder", "Mayfair High School", "California State University, Fullerton", 284);
        FootballPlayer ManningPeyton = new FootballPlayer("Peyton Manning", DenverBroncos, "Quarterback", "Isidore Newman School", "University of Tennessee", 965);
        SoccerPlayer DonnyJess = new SoccerPlayer("Jess Donny", DoaneTigersSoccer, "Guard", "Gresham High School", "Doane College", 503);
        TennisPlayer BamesburgerKyla = new TennisPlayer("Kyla Bamesburger", "Kearney High School", "Doane College", 4.0, 308);
        Golfer SuperiorNicholas = new Golfer("Nicholas Superior", "Lincoln Southeast High School", "Doane College", 12, 402);
        
        Teams.add(LADodgers);
        Teams.add(DenverBroncos);
        Teams.add(DoaneTigersSoccer);
        
        Players.add(KershawClayton);
        Players.add(TurnerJustin);
        Players.add(ManningPeyton);
        Players.add(DonnyJess);
        Players.add(BamesburgerKyla);
        Players.add(SuperiorNicholas);
        
        System.out.println("Teams: \n-----");
        for(Team t : Teams) {
            System.out.println(t.toString());
            t.listPlayers();
            System.out.println("");
        }
        
        System.out.println("\nPlayers: \n-----");
        for(Player p : Players) {
            System.out.println(p.toString());
        }
    }
    
}

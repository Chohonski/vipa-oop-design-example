/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Team;

import Player.Player;

/** Interfacing for a Team Roster.
 * Primarily handled by the Team superclass,
 * but implementations on an individual level
 * are always acceptable if needed.
 *
 * @author Mitch Chohon
 */
public interface Roster {
    public void addPlayer(Player player);
    public void removePlayer(Player player);
    public void listPlayers();
}

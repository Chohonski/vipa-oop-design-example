/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Team;
import Team.League.league;

/** Subclass for Soccer Teams
 *
 * @author Mitch Chohon
 */
public class SoccerTeam extends Team {
    
    private league soccerLeague;
    
    public SoccerTeam(String name, String town, String mascot) {
        super(name, town, mascot);
    }
    
    public SoccerTeam(String name, String town, String mascot, league soccerLeague) {
        super(name, town, mascot);
        this.soccerLeague = soccerLeague;
    }

    /**
     * @return the soccerLeague
     */
    public league getSoccerLeague() {
        return soccerLeague;
    }

    /**
     * @param soccerLeague the soccerLeague to set
     */
    public void setSoccerLeague(league soccerLeague) {
        this.soccerLeague = soccerLeague;
    }
    
    @Override
    public String toString() {
        return (name + " (Mascot: " + mascot + "'s) from " + town + " in league " + soccerLeague + ".");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Team;
import Team.League.league;

/** Subclass for Football Teams
 *
 * @author Mitch Chohon
 */
public class FootballTeam extends Team {
    
    private league footballLeague;
    
    public FootballTeam(String name, String town, String mascot) {
        super(name, town, mascot);
    }
    
    public FootballTeam(String name, String town, String mascot, league footballLeague) {
        super(name, town, mascot);
        this.footballLeague = footballLeague;
    }

    /**
     * @return the footballLeague
     */
    public league getFootballLeague() {
        return footballLeague;
    }

    /**
     * @param footballLeague the footballLeague to set
     */
    public void setFootballLeague(league footballLeague) {
        this.footballLeague = footballLeague;
    }
    
    @Override
    public String toString() {
        return (name + " (Mascot: " + mascot + "'s) from " + town + " in league " + footballLeague + ".");
    }
    
}

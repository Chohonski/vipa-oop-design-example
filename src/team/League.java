/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Team;

/** List of possible leagues that a team may be in.
 * Since I'm not really familiar with sports and leagues,
 * these are all just school class sizes.
 * 
 * @author Mitch Chohon
 */
public class League {
    public enum league {
        A, B, C1, C2, D1, D2
    }
}

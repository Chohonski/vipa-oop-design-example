/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Team;
import Player.Player;
import Team.League.league;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

/** Team superclass for sports teams.
 * Subclasses are free to implement their own
 * variables and statistics to track.
 * 
 * Few instances of teams without mascots
 * exist, but is handled by the superclass.
 *
 * @author Mitch Chohon
 */
public abstract class Team implements Roster {
    protected String name;
    protected String town;
    protected boolean hasMascot = true;
    protected String mascot;
    protected Date dateUpdated;
    protected boolean hasNotableAchievement;
    ArrayList<Player> players = new ArrayList<>();
    
    /**
     * @param name Team name
     * @param town Team's origin
     * @param mascot Team's mascot
     */
    protected Team(String name, String town, String mascot) {
        this.name = name;
        this.town = town;
        this.mascot = mascot;
        dateUpdated = Date.from(Instant.MIN);
        
        if(this.mascot.equals("")) {
            hasMascot = false;
        }
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the town
     */
    public String getTown() {
        return town;
    }

    /**
     * @param town the town to set
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     * @return the mascot
     */
    public String getMascot() {
        return mascot;
    }

    /**
     * @param mascot the mascot to set
     */
    public void setMascot(String mascot) {
        this.mascot = mascot;
        if(this.mascot.equals("")) {
            hasMascot = false;
        }
    }
    
    /** Flags the Player as having accomplished something notable.
     * This allows the user to mark players as eligible
     * for the Player of the Week.
     * 
     * @param flag True or False value to be set
     */
    public void setHasNotableAchievement(boolean flag) {
        this.hasNotableAchievement = flag;
        if(flag)
            dateUpdated = Date.from(Instant.MIN);
    }
    
    /** Adds a player to the team's roster
     * 
     * @param player The player to add to the team
     */
    @Override
    public void addPlayer(Player player) {
        players.add(player);
    }
    
    /** Removes a player from the team's roster
     * 
     * @param player The player to remove from the team
     */
    @Override
    public void removePlayer(Player player) {
        players.remove(player);
    }
    
    /**
     * Lists all of the players in the team's current roster
     */
    @Override
    public void listPlayers() {
        System.out.println(name + "'s Players:"); //Starts a printed list of the current team's players
        for(Player p : players) {
            System.out.println("\t> " + p.toString()); //Indents and bullets each player e.g. "   > Player info"
        }
    }
    
    @Override
    public String toString() {
        return (name + " (Mascot: " + mascot + "'s) from " + town + ".");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Team;
import Team.League.league;
import java.util.Observable;

/** Subclass for Baseball Teams
 *
 * @author Mitch Chohon
 */
public class BaseballTeam extends Team {
    
    private league baseballLeague;
    
    
    public BaseballTeam(String name, String town, String mascot) {
        super(name, town, mascot);
    }
    
    public BaseballTeam(String name, String town, String mascot, league baseballLeague) {
        super(name, town, mascot);
        this.baseballLeague = baseballLeague;
    }

    /**
     * @return the baseballLeague
     */
    public league getBaseballLeague() {
        return baseballLeague;
    }

    /**
     * @param baseballLeague the baseballLeague to set
     */
    public void setBaseballLeague(league baseballLeague) {
        this.baseballLeague = baseballLeague;
    }
    
    @Override
    public String toString() {
        if(hasMascot)
            return (name + " (Mascot: " + mascot + "'s) from " + town + " in league " + baseballLeague + ".");
        return (name + " from " + town + " in league " + baseballLeague + ".");
    }
    
}

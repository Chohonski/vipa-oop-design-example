/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Player;

import java.time.Instant;
import java.util.Date;

/** Superclass for handling players 
 *
 * @author Mitch Chohon
 */
public abstract class Player {
    protected String name;
    protected String highSchool;
    protected String college;
    protected int systemRating;
    protected int lastRating;
    protected int ratingDelta;
    protected boolean hasNotableAchievement;
    protected final String SPORT = "Sport support not supported. (Suppose you should support the sport field?)";
    protected Date dateUpdated;
    
    /**
     * @param name Player's name
     * @param highSchool Player's high school
     * @param college Player's college (if applicable)
     * @param systemRating Player's system rating (whatever that entails)
     */
    protected Player(String name, String highSchool, String college, int systemRating) {
        this.name = name;
        this.highSchool = highSchool;
        this.college = college;
        this.systemRating = systemRating;
        dateUpdated = Date.from(Instant.MIN);
    }
    
    /** Updates the player information with a new rating,
     * and logs what their older rating was.
     * 
     * Needs some kind of check to prevent it from being ran
     * too often, and producing players with very low Deltas.
     * Probably once per week?
     * 
     * @param rating The player's new rating
     */
    protected void updateRating(int rating) {
        lastRating = systemRating;
        systemRating = rating;
        getRatingDelta();
    }
    
    /** Returns the difference in a player's ratings
     * between two updates.
     * 
     * @return The change in a player's rating
     */
    public int getRatingDelta() {
        return ratingDelta = systemRating - lastRating;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the highSchool
     */
    public String getHighSchool() {
        return highSchool;
    }

    /**
     * @param highSchool the highSchool to set
     */
    public void setHighSchool(String highSchool) {
        this.highSchool = highSchool;
    }

    /**
     * @return the college
     */
    public String getCollege() {
        return college;
    }

    /**
     * @param college the college to set
     */
    public void setCollege(String college) {
        this.college = college;
    }

    /**
     * @return the systemRating
     */
    public int getSystemRating() {
        return systemRating;
    }
    
    /** Flags the Player as having accomplished something notable.
     * This allows the user to mark players as eligible
     * for the Player of the Week.
     * 
     * @param flag True or False value to be set
     */
    public void setHasNotableAchievement(boolean flag) {
        this.hasNotableAchievement = flag;
    }

    @Override
    public String toString() {
        return (name + " from " + highSchool + " at " + college + ", rated " + systemRating + ".");
    }
}

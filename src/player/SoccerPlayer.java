/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Player;
import Team.*;

/** Subclass for Soccer Players
 *
 * @author Mitch Chohon
 */
public class SoccerPlayer extends Player{
    private String teamName;
    private String teamPosition;
    private final String SPORT = "Soccer";
    
    /**
     * @param name Player's name
     * @param highSchool Player's high school
     * @param college Player's college (if applicable)
     * @param systemRating Player's system rating (whatever that entails)
     */
    public SoccerPlayer(String name, String highSchool, String college, int systemRating) {
        super(name, highSchool, college, systemRating);
    }
    
    /**
     * @param name Player's name
     * @param soccerTeam Player's current team
     * @param teamPosition Player's position on the team
     * @param highSchool Player's high school
     * @param college Player's college (if applicable)
     * @param systemRating Player's system rating (whatever that entails)
     */
    public SoccerPlayer(String name, SoccerTeam soccerTeam, String teamPosition, String highSchool, String college, int systemRating) {
        super(name, highSchool, college, systemRating);
        this.teamName = soccerTeam.getName();
        this.teamPosition = teamPosition;
        
        RegisterToTeam(soccerTeam);
    }
    
     /** Attempts to register said player to their given team.
     * A Null-Pointer Exception will be thrown if the team doesn't exist.
     * 
     * @param soccerTeam The team to register the player to
     */
    private void RegisterToTeam(SoccerTeam soccerTeam) {
        try {
            soccerTeam.addPlayer(this);
        } catch(NullPointerException npe) {
            System.out.println("Error: Soccer Team doesn't exist.");
        }
    }

    /**
     * @return the teamName
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * @param soccerTeam the team object to retrieve the name from
     */
    public void setTeamName(SoccerTeam soccerTeam) {
        this.teamName = soccerTeam.getName();
        RegisterToTeam(soccerTeam);
    }

    /**
     * @return the teamPosition
     */
    public String getTeamPosition() {
        return teamPosition;
    }

    /**
     * @param teamPosition the teamPosition to set
     */
    public void setTeamPosition(String teamPosition) {
        this.teamPosition = teamPosition;
    }
    
    /**
     * @return the SPORT
     */
    public String getSport() {
        return SPORT;
    }
    
    @Override
    public String toString() {
        return (name + " playing for " + teamName + " as " + teamPosition +  " from " + highSchool + " at " + college + ", rated " + systemRating + ". (Plays " + SPORT + ")");
    }
}

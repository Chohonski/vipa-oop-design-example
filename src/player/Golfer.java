/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Player;
import Team.*;

/** Subclass for Golfers
 *
 * @author Mitch Chohon
 */
public class Golfer extends Player{
    private int handicap;
    private final String SPORT = "Golf";
    
    /**
     * @param name Player's name
     * @param highSchool Player's high school
     * @param college Player's college (if applicable)
     * @param systemRating Player's system rating (whatever that entails)
     */
    public Golfer(String name, String highSchool, String college, int systemRating) {
        super(name, highSchool, college, systemRating);
    }
    
    /**
     * @param name Player's name
     * @param highSchool Player's high school
     * @param college Player's college (if applicable)
     * @param handicap Player's stroke handicap
     * @param systemRating Player's system rating (whatever that entails)
     */
    public Golfer(String name, String highSchool, String college, int handicap, int systemRating) {
        super(name, highSchool, college, systemRating);
        this.handicap = handicap;
    }

    /**
     * @return the handicap
     */
    public int getHandicap() {
        return handicap;
    }

    /**
     * @param handicap the handicap to set
     */
    public void setHandicap(int handicap) {
        this.handicap = handicap;
    }
    
    /**
     * @return the SPORT
     */
    public String getSport() {
        return SPORT;
    }
    
    @Override
    public String toString() {
        return (name +  " from " + highSchool + " at " + college + ", rated " + systemRating + " with a " + handicap + " handicap. (Plays " + SPORT + ")");
    }
}

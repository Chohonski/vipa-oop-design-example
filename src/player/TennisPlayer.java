/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Player;
import Team.*;

/** Subclass for Tennis Players
 *
 * @author Mitch Chohon
 */
public class TennisPlayer extends Player{
    private double ntrp;
    private final String SPORT = "Tennis";
    
    /**
     * @param name Player's name
     * @param highSchool Player's high school
     * @param college Player's college (if applicable)
     * @param systemRating Player's system rating (whatever that entails)
     */
    public TennisPlayer(String name, String highSchool, String college, int systemRating) {
        super(name, highSchool, college, systemRating);
    }
    
    /**
     * @param name Player's name
     * @param highSchool Player's high school
     * @param college Player's college (if applicable)
     * @param ntrp Player's National Tennis Rating Program level (1.0-7.0)
     * @param systemRating Player's system rating (whatever that entails)
     */
    public TennisPlayer(String name, String highSchool, String college, double ntrp, int systemRating) {
        super(name, highSchool, college, systemRating);
        this.ntrp = ntrp;
    }

    /**
     * @return the ntrp
     */
    public double getNtrp() {
        return ntrp;
    }

    /**
     * @param ntrp the ntrp to set
     */
    public void setNtrp(double ntrp) {
        this.ntrp = ntrp;
    }
    
    /**
     * @return the SPORT
     */
    public String getSport() {
        return SPORT;
    }
    
    @Override
    public String toString() {
        return (name +  " from " + highSchool + " at " + college + ", rated " + systemRating + " with a " + ntrp + " NTRP. (Plays " + SPORT + ")");
    }
}
